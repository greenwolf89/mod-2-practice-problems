// Functional Array methods
// ALL methods apply a function to each item within your array
​
const numbers = [1,2,3,4];
console.log(numbers.map(n => n*2));
// map TRANSFORMS an array, returning a new value based on the function passed in.
​
console.log(numbers.filter(n => n%2 === 0));
// filter REFINES an array, returning an existing item if the passed function is true
​
​
// let total = 0;
// function addToTotal(total, num) {
//     return total + num;
// }
​
// for (num of numbers) {
//     total = addToTotal(total, num);
// }
// console.log(total);
​
console.log(numbers.reduce((total, num) => total + num), 0)
// reduce RETURNS AN ACCUMULATION of an array, returning a means to change your accumulator using the function passed in
​
const cart = [
    {
        "name": "coffee",
        "cost": 2.50,
        "is_liquid": true
    },
    {
        "name": "danish",
        "cost": 3.00,
        "is_liquid": false
    },
    {
        "name": "water",
        "cost": 1.00,
        "is_liquid": true
    }
]
​
//Calculate the total cost of all items
console.log(cart.reduce((total_cost, item) => (total_cost + item.cost), 0));
​
//Calculate the total amount of liquid items
console.log(cart.reduce((total_liquids, item) => {
    if (item.is_liquid) {
        return(total_liquids + 1);
    } else {
        return(total_liquids)
    }
}, 0));
​
//Calculate the total cost of all liquid items
console.log(cart.reduce((liquid_costs, item) => {
    if (item.is_liquid) {
        return(liquid_costs + item.cost);
    } else {
        return(liquid_costs)
    }
}, 0));
​
//Show a list of the names of all liquid items
console.log(cart.reduce((liquid_names, item) => {
    if (item.is_liquid) {
        return([...liquid_names, item.name]);
    } else {
        return(liquid_names)
    }
}, []));
