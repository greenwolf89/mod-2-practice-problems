function objectFromPairs(pairs) {
    let obj = {};
    for (let p of pairs){
        obj[p[0]] = p[1];
        }
    return obj;
}
