function hasProperty(obj, propertyName) {
    return obj[propertyName] != undefined;
}
