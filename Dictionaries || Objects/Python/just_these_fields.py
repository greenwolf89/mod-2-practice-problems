def just_these_fields(items, fields):
    result = []
    for old_d in items:
        d = {}
        for key in fields:
            if key in old_d:
                d[key] = old_d[key]
            else:
                pass
        result.append(d)
    return result

items = [
    {"a": 1, "b":2, "c": 3},
    {"a":3, "size": 4},
    {"b": 5, "d": 7}
]
fields = ["a", "b"]
result = just_these_fields(items, fields)
print(result) # -->
# [
#     {"a": 1, "b": 2},
#     {"a":3},
#     {"b": 5}
# ]
