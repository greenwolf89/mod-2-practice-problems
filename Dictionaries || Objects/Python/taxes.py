def taxes(bill, tax_rate):
    taxables = [line_item for line_item in bill if line_item["taxable"] is True]
    return tax_rate * sum(line_item["quantity"] * line_item["item_cost"] for line_item in taxables)
