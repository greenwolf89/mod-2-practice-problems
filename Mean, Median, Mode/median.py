def median(numbers):
    numbers.sort()
    if len(numbers) % 2 == 0:
        return (numbers[len(numbers)//2-1] + numbers[len(numbers)//2]) / 2
    return numbers[len(numbers)//2]
