def mode(numbers):
    counts = {}
    for n in numbers:
        counts[n] = counts.get(n,0) + 1
    return max(counts, key=counts.get)
