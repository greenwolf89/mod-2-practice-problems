function countAllCharacters(string) {
    if (string.length === 0) {
        return {};
    }
    let counts = {};
    for (let i=0; i < string.length; i++) {
        let char = string[i];
        if (counts[char] === undefined) {
            counts[char] = 1;
        } else {
            counts[char]++;
        }
    } return counts;
  }
