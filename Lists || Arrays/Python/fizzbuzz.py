def fizzbuzz(maximum):
    if maximum == 0:
        return []
    else:
        nums = list(range(1,maximum + 1))
        res = []
        for n in nums:
            if n % 3 == 0 and n % 5 == 0:
                res.append("fizzbuzz")
            elif n % 3 == 0:
                res.append("fizz")
            elif n % 5 == 0:
                res.append("buzz")
            else:
                res.append(str(n))
    return res
