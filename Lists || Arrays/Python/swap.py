def swap(items, idx_1, idx_2):
    (items[idx_1], items[idx_2]) = (items[idx_2], items[idx_1])
    return items
