def sort_by_make(cars):
    res = {}
    for car in cars:
        make = car["make"]
        if res.get(make) is None:
            res[make] = []
        res[make].append(car)


    return res

cars = [
    {"make": "ford", "model": 8, "year": 1955},
    {"make": "chevy", "model": 12, "year": 1964},
    {"make": "ford", "model": 11, "year": 1978},
    {"make": "chevy", "model": 19, "year": 2000},
]

print(sort_by_make(cars))
