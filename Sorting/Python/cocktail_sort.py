def cocktail_sort(items):
    while True:
        made_a_swap = False

        for i in range(len(items)-1):
            if items[i] > items[i+1]:
                (items[i], items[i+1]) = (items[i+1], items[i])
                made_a_swap = True
        if not made_a_swap:
            break

        made_a_swap = False
        for i in range(len(items)-1,0,-1):
            if items[i] < items[i-1]:
                (items[i], items[i-1]) = (items[i-1], items[i])
                made_a_swap = True

        if not made_a_swap:
            break

    return items
